import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(

    public router:Router,
    public api:ApiService
  ) { }

  ngOnInit(): void {
  }

  user: any={};
  hide:boolean=true;

  //form validation
  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', [Validators.required]);

  loading:boolean | undefined;
  login(user:any)
  {
    this.loading=true;
    this.api.login(this.user.email, this.user.password).subscribe(res=>{
      console.log(res);
      this.loading=false;
      alert('Login Berhasil');
      localStorage.setItem('appToken',JSON.stringify(res));
      this.router.navigate(['admin/dashboard']);
    },error=>{
      this.loading=false;
      alert('Tidak dapat login');
  });
}



}
